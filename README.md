# GRay_Mygrate

## Preambule

It is necessary to start with the fact that this repo was forked by this code [Mygrate](https://code.google.com/archive/p/mygrate/)

I don't know, who is original developer, but i need this utility for my job. Unfortunately, there is no even nickname.

## About

It is utility for storing structure migration of your DB(MySQL) tightly
integrated with Mercurial. It looks like directory on your repository (examp.,
"Migrations/"), where stores your migrations in \*.sql files

Pay attention: some features wasn't done. I gonna complete that, but i havn't enough time.

## Requirements

+ MySQL
+ Original code support Python 2.5 (should also work with earlier versions).
I developing on Python 2.7.
+ Also you can install Mercurial (mygrate can auto-add files to commit).

## Installation

1. Clone this repo to local directory
2. Run utility by mygrate.py like

        ./mygrate.py

3. You can make symlink to this file on /usr/share/bin/

        ln -s ./mygrate.py /usr/share/bin/mygrate

## What have i done:

+ Refactoring (aims to PEP8)
+ Exclude(hardcode) warning about -p from errors
  (It crashes every time, when it use mysql)
+ Some fixes in log and update (i was drunk & i didn't use git yet)
