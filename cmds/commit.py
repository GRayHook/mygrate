#!/usr/bin/env python
"""Commit"""

import sys
import db.dump
from db.table import Database
import repo
import repo.revision
import repo.migration
import cmds.init

def run(args):
    """Commits current repository changes."""
    cmds.init.require_init()
    repo.allow_if_at_tip()
    try:
        message = args[0]
    except IndexError:
        print """== No message specified. Using the following message to commit: ==\n"""
        message = auto_commit_message()
        print message
    print "Commiting current changes."

    src = Database().parse_string(repo.revision.latest())
    dest = Database().parse_string(db.dump.dump())

    if src == dest:
        print """No changes, nothing to commit."""
        sys.exit()

    number = repo.revision.latest_number() + 1
    repo.migration.add(number, dest - src, src - dest, message)
    repo.revision.save()

    print """Migration #%s successfully created.""" % number

    if cmds.init.get_config()["vcs"] == "hg":
        add_to_hg(number)


def add_to_hg(number):
    """Add to index to commit in Mercurial"""
    migration = repo.migration.Migration(number)
    migration.add_to_hg()

def auto_commit_message():
    """Generate text for commit message"""
    diff = Database().parse_string(db.dump.dump()) - Database().parse_string(repo.revision.latest())
    commit_msg = []
    for tbl in diff.tables['added']:
        commit_msg.append("Added `%s`" % tbl.name)
    for tbl in diff.tables['dropped']:
        commit_msg.append("Dropped `%s`" % tbl.name)
    for (tbl, dest_tbl) in diff.tables['modified']:
        commit_msg.append("Modified `%s`:" % tbl.name)
        diff_st = dest_tbl - tbl
        for field in diff_st.fields['added']:
            commit_msg.append("    + added `%s`" % field.name)
        for field in diff_st.fields['dropped']:
            commit_msg.append("    - dropped `%s`" % field.name)
        for field in diff_st.fields['modified']:
            commit_msg.append("        modified `%s` to %s%s"\
                              % (field.name, field.type,\
                              "(%s)"\
                              % field.options if field.options else ""))
    return "\n".join(commit_msg)
