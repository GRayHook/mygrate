#!/usr/bin/env python
"""Tip"""

from optparse import OptionParser
import repo.history
import cmds.init
# import sys
# import db.dump
# from db.table import Database

def run(args):
    """Show tip change of a repository."""
    cmds.init.require_init()
    args = optargs(args)[1]
    history = repo.history.load()
    print history.tip()

def optargs(args):
    """Parses options for current command."""
    parser = OptionParser()
    parser.add_option("-r", "--rev", dest="revision",\
                      help="Start from revision number")
    (options, args) = parser.parse_args(args)
    return (options, args)
