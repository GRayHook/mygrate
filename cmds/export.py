#!/usr/bin/env python
"""Export"""

from optparse import OptionParser
import cmds.init
from db.table import Database
import db.dump
# import repo
# import utils

def run(args):
    """Exports table data to a given format."""
    cmds.init.require_init()
    (options, args) = optargs(args)
    current = Database().parse_string(db.dump.dump())
    table = current.table(options.table)
    export_format = options.format or "json"
    if export_format == "model":
        as_model(table)

def as_model(table):
    """Export as model"""
    fields = [f.name for f in table.fields]
    print """<?php

class %s extends ZFast_Model_goDB {

    public $class = __CLASS__;
    public $View;

    public %s;

    const ON_PAGE = 10; const TABLE = '%s';
    const PRIMARY = 'id'; const ORDER_BY = '`id` DESC';

    public function setupFields() {
%s
    } """ % (table.name, ", ".join(["$%s" % f for f in fields]), table.name,\
        "\n".join(["            $this->addField ('%s');" % f for f in fields[1:]])\
    )

    # real_fields = fields[1:]
    # model_fields = [f for f in real_fields if f.endswith('_id')]
    # NOTE: I think, here isn't any complete things
    print """     /* Static functions */

    public static function __create (User $User) {
            $I = new self();
            $I->user_id = $User->id;
            $I->date = date ('Y-m-d H:i:s');
            $I->save();
            return $I;
    }"""

    print """
    /**
     * @param int $id
     */
    public static function byId($id) {
        return parent::byId(__CLASS__, $id);
    }

}"""

def optargs(args):
    """Parses options for current command."""
    parser = OptionParser()
    parser.add_option("-t", "--table", dest="table",\
                      help="Table to export")
    parser.add_option("-f", "--format", dest="format",\
                      help="Format to export (available: json)")

    (options, args) = parser.parse_args(args)
    return (options, args)
