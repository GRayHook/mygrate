#!/usr/bin/env python
"""Status"""

import sys
from optparse import OptionParser
import db.dump
from db.table import Database
import repo.revision
import repo
import cmds.init

def run(args):
    """Show changed tables since latest commit."""
    cmds.init.require_init()

    (options, args) = optargs(args)

    revision = repo.revision.latest_number() if not options.revision else int(options.revision)

    repo.allow_if_at_tip(die=False)

    src = Database().parse_string(repo.revision.by_number(revision))
    dest = Database().parse_string(db.dump.dump())
    if src == dest:
        print """No changes since revision %s.""" % (revision)
        sys.exit()
    diff = dest - src
    status = {}
    info = {}

    for tbl in diff.tables['added']:
        status[tbl.name] = "A"
    for tbl in diff.tables['dropped']:
        status[tbl.name] = "R"
    for (tbl, dest_tbl) in diff.tables['modified']:
        status[tbl.name] = "M"
        if dest_tbl > tbl:
            status[tbl.name] = "M+"
        if dest_tbl < tbl:
            status[tbl.name] = "M-"
        diff_st = dest_tbl - tbl
        info[tbl.name] = {}
        for field in diff_st.fields['added']:
            info[tbl.name][field.name] = "+"
        for field in diff_st.fields['dropped']:
            info[tbl.name][field.name] = "-"
        for field in diff_st.fields['modified']:
            info[tbl.name][field.name] = "m"


    for tbl in sorted(status.keys()):
        print " %s %s" % (status[tbl].ljust(2, " "), tbl)
        if not tbl in info.keys():
            continue
        for key in info[tbl].keys():
            print "        %s %s" % (info[tbl][key], key)

def optargs(args):
    """Parses options for current command."""
    parser = OptionParser()
    parser.add_option("-r", "--rev", dest="revision",\
                      help="Revision to compare current status to")
    (options, args) = parser.parse_args(args)
    return (options, args)
