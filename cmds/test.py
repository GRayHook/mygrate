#!/usr/bin/env python
"""Test changes"""

import sys
import db.dump
from db.table import Database
import repo.revision
import cmds.init

def run():
    """Test current repository changes."""
    cmds.init.require_init()
    print "Testing current changes."
    src = Database().parse_string(repo.revision.latest())
    dest = Database().parse_string(db.dump.dump())

    restore = db.dump.restore_point()

    if src == dest:
        print """No changes, nothing to test."""
        sys.exit()

    test_up = str(dest - src)
    test_down = str(src - dest)

    print """Downgrading..."""
    out = db.dump.load(test_down)
    print """MySQL said: %s""" % out

    print """Upgrading..."""
    out = db.dump.load(test_up)
    print """MySQL said: %s""" % out

    new_state = Database().parse_string(db.dump.dump())
    if new_state == dest:
        print """DB is stable, changes are ready for migration."""
        sys.exit()

    print """There were unexpected changes:"""
    print new_state - dest
    print """Attempting automatic restore"""
    out = db.dump.load(restore)
    print """MySQL said: %s""" % out
