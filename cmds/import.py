#!/usr/bin/env python
"""Import"""

import db
import db.dump
import cmds.init

def run(args):
    """Imports a SQL file to the database."""
    cmds.init.require_init()
    if not args:
        print "Specify an SQL file to run."
    filename = args[0]
    try:
        sql = open(filename, 'rb').read()
    except IOError:
        print "Can't find file: %s" % file
        return
    try:
        db.dump.load(sql)
        print """SQL file imported. You can `commit` or `revert`."""
    except db.SQLLoadError, exception:
        print "Can't import file: %s." % file
        print exception
