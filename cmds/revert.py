#!/usr/bin/env python
"""Revert"""

import sys
from optparse import OptionParser
import db.dump
from db.table import Database
import cmds.init
import repo.revision

def run(args):
    """Reverts repository to its last commited state."""
    cmds.init.require_init()
    (options, args) = optargs(args)
    if not args:
        print "Comparing current database state to latest commited revision."
        src = Database().parse_string(repo.revision.latest())
        dest = Database().parse_string(db.dump.dump())
        revert_sql = str(src - dest)
        if revert_sql == '':
            print "Nothing to revert since revision #%s." % repo.revision.latest_number()
            sys.exit()
        print """Reverting to last committed revision: #%s.""" % repo.revision.latest_number()
        if not options.debug:
            db.dump.load(revert_sql)
            return
        for line in revert_sql.split("\n"):
            print line.strip() + "\n"
            print db.dump.load(line.strip())

def optargs(args):
    """Parses options for current command."""
    parser = OptionParser()
    parser.add_option("-d", "--debug", dest="debug", action="store_true",\
                      default=False,\
                      help="Run revert line-by-line with debug info")
    (options, args) = parser.parse_args(args)
    return (options, args)
