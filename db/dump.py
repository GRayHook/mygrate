#!/usr/bin/env python
# coding: utf-8
"""Dump"""

from subprocess import Popen, PIPE
import os
import datetime
import cmds.init
from db import SQLLoadError



def restore_point():
    """Restore point"""
    config = cmds.init.get_config()
    user = config["db_user"]
    password = config["db_pass"]
    db_name = config["db_db"]
    host = config["db_host"]
    return mysqldump_command(\
        "-u%s %s%s %s --add-drop-table --default-character-set=utf8 %s"\
        % (user,\
           "-p" if password else "", password,\
           "-h"+host if host <> 'localhost' else "",\
           db_name))

def dump():
    """Dump db"""
    config = cmds.init.get_config()
    user = config["db_user"]
    password = config["db_pass"]
    db_name = config["db_db"]
    host = config["db_host"]
    return mysqldump_command(\
           "--no-data --add-locks=false --compact -u%s %s%s %s --default-character-set=utf8 %s"\
           % (user,\
           "-p" if password else "", password,\
           "-h"+host if host <> 'localhost' else "",\
           db_name))

def load(sql):
    """Run sql on db"""
    config = cmds.init.get_config()
    user = config["db_user"]
    password = config["db_pass"]
    db_name = config["db_db"]
    host = config["db_host"]

    tempfile = ".temp-mygrate-%s" % str(datetime.time()).replace(':', '_')
    temp_fd = open(tempfile, 'w')
    temp_fd.write(sql)
    temp_fd.close()

    errors = mysql_command("-u%s %s%s %s --default-character-set=utf8 %s < %s"\
        % (user,\
        "-p" if password else "", password,\
        "-h"+host if host <> 'localhost' else "",\
        db_name,\
        tempfile))
    os.unlink(tempfile)
    if errors:
        if errors != ("mysql: [Warning] Using a password on ",\
                      "the command line interface can be insecure.\n"):
            raise SQLLoadError(sql=sql, errors=errors)
    return True

def mysql_command(cmd):
    """Run mysql"""
    config = cmds.init.get_config()
    mysql_path = config["mysql"] if "mysql" in config else "mysql"
    process = Popen("%s %s" % (mysql_path, cmd), shell=True, stdout=PIPE, stderr=PIPE)
    # out = process.stdout.read()
    err = process.stderr.read()
    # return (out, err)
    return err

def mysqldump_command(cmd):
    """Run mysqldump"""
    config = cmds.init.get_config()
    mysqldump_path = config["mysqldump"] if "mysqldump" in config else "mysqldump"
    process = Popen("%s %s" % (mysqldump_path, cmd), shell=True, stdout=PIPE)
    out = process.stdout.read()
    return out
