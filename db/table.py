#!/usr/bin/env python
# coding: utf-8
# pylint: disable=R0903
"""Table"""
import sys
import re
import db.alter as alter

def main(file_fd):
    """Body"""
    database = Database()
    database.parse_file(file_fd)
    print database

class Database(object):
    """Class of DB"""
    def __init__(self):
        self.tables = []
        self.__table_names = []

    def __sub__(self, other):
        return alter.DatabaseDiff().compare(other, self)

    def __eq__(self, other):
        return not self != other

    def __ne__(self, other):
        return bool(str(self - other))

    def __str__(self):
        return "\n\n".join([str(table) for table in self.tables if str(table)])

    def parse_file(self, file_fd):
        """Get info from file"""
        try:
            string = open(file_fd, 'r').read()
        except IOError:
            print "Can't open file %s." % file_fd
            sys.exit()
        return self.parse_string(string)

    def parse_string(self, string):
        """Parses a file and returns an instance of Database."""
        string = string.replace("\r\n", "\n")
        return self.parse_lines(string.splitlines(True))

    def parse_lines(self, lines):
        """Parse file's info by line"""
        # lines = [line for line in lines if not line.startswith("SET ")\
        #               and not line.startswith("--") and not line.startswith('/*')\
        #               and line != "\n"]
        content = []
        for line in lines:
            boolka = not line.startswith("SET ") and not line.startswith("--")\
                     and not line.startswith('/*') and line != "\n"
            if boolka:
                content.append(line)
        querys = []
        query = []
        for line in content:
            query.append(line)
            # @type line str
            if line.endswith(";\n"):
                querys.append(query)
                query = []
        tables = [Table(query) for query in querys]
        self.tables = tables
        self.__table_names = [table.name for table in self.tables]
        return self

    def table(self, name):
        """Get table if it exists by name"""
        if self.table_exists(name):
            return self.tables[self.__table_names.index(name)]
        return None

    def table_exists(self, table):
        """Check table for existing"""
        table_name = table.name if isinstance(table, Table) else table
        if table_name in self.__table_names:
            return True
        return False



class Table(object):
    """Class containing base info for MySQL tables."""
    def __init__(self, data):
        """Initialiazes a Table class from SQL data."""
        self.name = ''
        self.fields = []
        self.primary_index = ''
        self.indexes = []
        self.options = {}

        self.__field_names = []
        self.__index_names = []

        matchs = re.search(r"CREATE TABLE `(.+?)`", data[0])
        self.name = matchs.group(1)
        options = re.finditer(r"([\w\d_ ]+?)=([^\s]+)", data[-1].strip()[2:-1])
        for opts_tuples in options:
            self.options[opts_tuples.group(1).strip()] = opts_tuples.group(2)
        fields_and_indexes = [line.strip() for line in data[1:-1]]
        for field_or_index in fields_and_indexes:
            if field_or_index.startswith("PRIMARY"):
                name = re.search("`(.+?)`", field_or_index).group(1)
                self.primary_index = name
                continue
            if field_or_index.startswith("`"):
                self.fields.append(Field(field_or_index))
                continue
            self.indexes.append(Index(field_or_index))

        self.__field_names = [field.name for field in self.fields]
        self.__index_names = [index.name for index in self.indexes]

    def __sub__(self, other):
        return alter.TableDiff().compare(other, self)

    def __eq__(self, other):
        return not self != other

    def __ne__(self, other):
        return bool(str(self - other))

    def __gt__(self, other):
        return len(self.fields) > len(other.fields)

    def __lt__(self, other):
        return len(self.fields) < len(other.fields)

    def field(self, name):
        """Get field if it exists"""
        if name not in self.__field_names:
            return None
        return self.fields[self.__field_names.index(name)]

    def field_exists(self, field):
        """Check field for existing"""
        return field.name in self.__field_names

    def index(self, name):
        """Get name's inedex"""
        if not name in self.__index_names:
            return None
        return self.indexes[self.__index_names.index(name)]

    def index_exists(self, index):
        """Check index for existing"""
        if index.name in self.__index_names:
            return True
        return False

    def __str__(self):
        return self.generate_query()

    def generate_query(self):
        """Generate SQL-query of object"""
        return """CREATE TABLE `%s` (
%s
    PRIMARY KEY (`%s`)%s
    %s
) %s;""" % (self.name, "\n".join(["    %s," % str(f) for f in self.fields]),\
            self.primary_index,\
            "," if self.indexes else "",\
            ",\n    ".join(["%s" % str(i) for i in self.indexes]),\
            " ".join(["%s=%s"\
            % (k, self.options[k]) for k in self.options if k != "AUTO_INCREMENT"]))

    def generate_drop_query(self):
        """Generate query for dropping table"""
        return """DROP TABLE `%s`;""" % (self.name)

class Field(object):
    """Field of table"""
    def __init__(self, data):
        self.name = ''
        self.type = ''
        self.options = ''
        self.null = True
        self.auto_increment = False
        self.default = ''
        name = re.search(r"^`(.+?)`", data).group(1)
        self.name = name
        data = data[len("`%s` " % self.name):]
        match = re.search(r"^(.+?)(\((.+?)\))?\s(.+)$", data)
        if match:
            self.type = match.group(1)
            self.options = match.group(3)
            data = match.group(4)
        else:
            match = re.search(r"^([A-z]+)(\s*)(.*)$", data)
            self.type = match.group(1)
            data = match.group(3)

        if data.startswith("NOT NULL"):
            self.null = False
        match = re.search(r"default '(.+?)'", data)
        if match:
            self.default = "'%s'" % match.group(1)
        else:
            match = re.search(r"(?:default|DEFAULT) ('.+?'|[^\s,]+)", data)
            if match:
                self.default = "%s" % match.group(1)
        match = re.search(r"auto_increment|AUTO_INCREMENT", data)
        if match:
            self.auto_increment = True

    def __ne__(self, other):
        return (self.name != other.name) | (self.type != other.type) |\
               (self.options != other.options) | (self.null != other.null) |\
               (self.auto_increment != other.auto_increment) |\
               (self.default != other.default)

    def __eq__(self, other):
        return not self != other

    def __str__(self):
        return "`%s` %s%s%s%s%s" % (self.name, self.type,\
               "(%s)" % self.options if self.options else "",\
               ' NULL' if self.null else " NOT NULL",\
               " default %s" % self.default if self.default else "",\
               ' auto_increment' if self.auto_increment else ""\
               )

class Index(object):
    """Index of table"""
    def __init__(self, data):
        self.type = ''
        self.name = ''
        self.fields = ''
        match = re.search(r"^(.+?)\s+`(.+?)`\s+\((.+?)\)", data)
        self.type = match.group(1)
        self.name = match.group(2)
        self.fields = match.group(3)

    def __ne__(self, other):
        return (self.name != other.name) | (self.type != other.type) |\
               (self.fields != other.fields)

    def __eq__(self, other):
        return not self != other

    def __str__(self):
        return "%s `%s` (%s)" % (self.type, self.name, self.fields)

    def for_alteration(self):
        """Generate some string(i dont know, fuck this)"""
        types = {
            'KEY': 'INDEX',
            'UNIQUE KEY': 'UNIQUE',
            'FULLTEXT KEY': 'FULLTEXT'
        }
        return "%s `%s` (%s)" % (types[self.type], self.name, self.fields)

if __name__ == '__main__':
    main('../source.sql')
