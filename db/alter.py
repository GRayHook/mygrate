# coding: utf-8
# pylint: disable=R0903
"""alter"""
class DatabaseDiff(object):
    """DB diff"""
    def __init__(self):
        self.tables = {
            'added': [],
            'dropped': [],
            'modified': []
        }

    def compare(self, src, dest):
        """Compare DBs"""
        for table in src.tables:
            if not dest.table_exists(table):
                self.tables['dropped'].append(table)
                continue
            if dest.table(table.name) != table:
                self.tables['modified'].append((table, dest.table(table.name)))

        for table in dest.tables:
            if not src.table_exists(table):
                self.tables['added'].append(table)

        return self

    def __str__(self):
        out = []
        for table in self.tables['added']:
            out.append(str(table))
        for table in self.tables['dropped']:
            out.append(table.generate_drop_query())
        for (srctable, desttable) in self.tables['modified']:
            out.append(str(desttable - srctable))
        return "\n".join(out)

class TableDiff(object):
    """Diff tables"""
    def __init__(self):
        self.name = ''
        self.fields = {
            'added': [],
            'dropped': [],
            'modified': []
        }
        self.indexes = {
            'added': [],
            'dropped': [],
            'modified': []
        }
        self.primarykey = {
            'added': [],
            'dropped': [],
            'modified': []
        }
        self.options = {
            'added': [],
            'dropped': [],
            'modified': []
        }

    def compare(self, src, dest):
        """Compare tables"""
        self.name = src.name

        # Fields
        for field in src.fields:
            if not dest.field_exists(field):
                self.fields['dropped'].append(field)
                continue
            if dest.field(field.name) != field:
                self.fields['modified'].append(dest.field(field.name))

        prev = None

        for field in dest.fields:
            if not src.field_exists(field):
                self.fields['added'].append((field, prev))
            prev = field.name

        # Indexes
        for index in src.indexes:
            if not dest.index_exists(index):
                self.indexes['dropped'].append(index)
                continue
            if dest.index(index.name) != index:
                self.indexes['modified'].append(dest.index(index.name))

        for index in dest.indexes:
            if not src.index_exists(index):
                self.indexes['added'].append(index)


        return self

    def __str__(self):
        out = []

        # Dropping indexes should go first.
        for index in self.indexes['dropped']:
            out.append("""ALTER TABLE `%s` DROP INDEX `%s`;"""\
                       % (self.name, index.name))

        for field in self.fields['dropped']:
            out.append("""ALTER TABLE `%s` DROP `%s`;"""\
                       % (self.name, field.name))

        for field in self.fields['modified']:
            out.append("""ALTER TABLE `%s` CHANGE `%s` %s;"""\
                       % (self.name, field.name, field))

        for (field, prev) in self.fields['added']:
            out.append("""ALTER TABLE `%s` ADD %s%s;"""\
                       % (self.name, field, " FIRST"\
                       if prev is None else " AFTER `%s`"\
                       % prev))

        for index in self.indexes['modified']:
            out.append("""ALTER TABLE `%s` DROP INDEX `%s` ADD %s;"""\
                       % (self.name, index.name, index.for_alteration()))

        for index in self.indexes['added']:
            out.append("""ALTER TABLE `%s` ADD %s;"""\
                       % (self.name, index.for_alteration()))

        return "\n".join(out)
