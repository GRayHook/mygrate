#!/usr/bin/env python
"""Repo's history"""

# import sys
import cPickle
import os.path
# import cmds.init
# import db.dump
import repo
import repo.migration
# NOTE: Imports may be needed, checkout it

HISTORY_PATH = ".mygrate/store/history"
HISTORY_PATH = os.path.join(repo.CUR_REPO.get_path(), HISTORY_PATH)

def load():
    """Get History"""
    return History()

def refresh():
    """Refresh stored history"""
    History().get_fresh_from_migrations().save()

class History(object):
    """History"""
    def __init__(self):
        self.history = {}
        # try:
        #     self.history = cPickle.load(open(HISTORY_PATH))
        # except Exception as e:
        #     print e.strerror
        # NOTE: it cant load unavailable file(where it )
        current_history = self.history
        self.get_fresh_from_migrations()
        if current_history != self.history:
            self.save()


    def save(self):
        """Dump history"""
        #/TODO: Find out why it doesnt write history to file
        cPickle.dump(self.history, open(HISTORY_PATH, 'w'))

    def get_fresh_from_migrations(self, force_refresh=False):
        """Refresh stored history if there is new migrations"""
        for migration_number in repo.migration.return_all():
            if not force_refresh and self.history.has_key(migration_number):
                continue
            self.history[migration_number] = repo.migration.Migration(migration_number)
            del self.history[migration_number].state_to_rollback
        return self

    def __str__(self):
        return self.out()

    def tip(self):
        """Get history for ending of branch"""
        return self.out(revision_from=max(self.history.keys()))

    def out(self, revision_to=None, revision_from=0, reversing=False):
        """Generate text"""
        text = []
        iterlist = sorted(self.history.keys(), reverse=True)
        if revision_to is None:
            revision_to = max(self.history.keys())
        if revision_to < revision_from:
            (revision_to, revision_from) = (revision_from, revision_to)
            reversing = not reversing
        iterlist = [v for (k, v) in enumerate(iterlist) if v >= revision_from and v <= revision_to]
        if reversing:
            iterlist = reversed(iterlist)
        for k in iterlist:
            migration = self.history[k]
            text.append("changeset:     %s\nuser:                "\
                        % (migration.info['migration']),\
                        "%s\ndate:                %s\nsummary:         %s"\
                        % (migration.info['author'],\
                           migration.info['date'], migration.info['message']))
        return "\n\n".join(text) + "\n"


def row_format(migration):
    """Some deprecated func"""
    #/TODO: Delete this
    newrow = {}
    newrow["no"] = migration["number"]
    # newrow['user'] = row["user"]
    # newrow["summary"] = row["summary"]
    # newrow["date"] = row["date"]
    return newrow
