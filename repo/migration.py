#!/usr/bin/env python
"""Migrations"""

import os
import os.path
from subprocess import Popen, PIPE
import datetime
import cmds.init
import repo
from repo.configobj import ConfigObj
import db.dump
from db import SQLLoadError, MigrationFailedError

def add(number, sql_up, sql_down, message):
    """Adds a new migration."""
    config = cmds.init.get_config()
    migration_dir = config["migrations_dir"]
    migration_dir = os.path.join(repo.CUR_REPO.get_path(), migration_dir)
    path = migration_dir + "/%s-%s.conf"

    unstripped_number = str(number).zfill(3)
    message_alias = message.lower().replace(' ', '_').replace('.', '').\
                    replace('`', '').replace("\n", '').replace(':', '')[0:16]
    revision = ConfigObj(path % (unstripped_number, message_alias))
    revision["number"] = number
    revision["message"] = message
    revision["up"] = sql_up
    revision["down"] = sql_down
    revision["author"] = current_user()
    revision["date"] = str(datetime.datetime.today())
    revision.write()

def return_all():
    """Return all available migrations.
    @TODO And what shall we do if migrations are not consecutive?"""
    return [m+1 for m in range(latest_number())]

def latest_number():
    """Returns greates migration number available."""
    config = cmds.init.get_config()
    migration_dir = config["migrations_dir"]
    migration_dir = os.path.join(repo.CUR_REPO.get_path(), migration_dir)

    migration_numbers = [int(file_fd[0:3]) for file_fd in os.listdir(migration_dir)]
    if not migration_numbers:
        return 0
    return max(migration_numbers)

def current_user():
    """Returns current user."""
    for user_variable in ['USER', 'USERNAME']:
        if user_variable in os.environ:
            return os.environ[user_variable]
    return "Unknown user"

class Migration(object):
    """Class for migrations"""
    def __init__(self, number):
        """Create a new migration"""
        config = cmds.init.get_config()
        migration_dir = config["migrations_dir"]
        migration_dir = os.path.join(repo.CUR_REPO.get_path(), migration_dir)

        self.info = {
            'number': '',
            'message': '',
            'sql': {'up': '', 'down': ''},
            'filename': '',
            'author': '',
            'date': ''
        }
        self.state_to_rollback = db.dump.restore_point()

        unstripped_number = str(number).zfill(3)
        filename = [file_fd for file_fd in os.listdir(migration_dir)\
                    if file_fd.startswith(unstripped_number)][0]
        self.__from_file(migration_dir + os.sep + filename)

    def __from_file(self, filename):
        """Create migration object from migration file."""
        config = ConfigObj(filename)
        self.info['migration'] = config["number"]
        self.info['message'] = config["message"]
        self.info['sql']['up'] = config["up"]
        self.info['sql']['down'] = config["down"]
        self.info['filename'] = filename
        if "date" in config:
            self.info['date'] = config["date"]
        else:
            config["date"] = self.info['date']\
                           = str(datetime.datetime.fromtimestamp(os.path.getctime(filename)))
            config.write()

        if "author" in config:
            self.info['author'] = config["author"]
        else:
            config["author"] = self.info['author'] = current_user()
            config.write()

    def up_action(self):
        """Executes the up action for migration"""
        try:
            db.dump.load(self.info['sql']['up'])
        except SQLLoadError, error:
            self.rollback(verbose=True, message=error)

    def down_action(self):
        """Executes the down action for migration"""
        try:
            db.dump.load(self.info['sql']['down'])
        except SQLLoadError, error:
            self.rollback(verbose=True, message=error)

    def delete(self):
        """Deletes current migration file"""
        os.unlink(self.info['filename'])

    def add_to_hg(self):
        """Add to Mercurial version control."""
        print """Adding migration to Mercurial..."""
        output = "hg add %s" % (self.info['filename'])
        output = Popen(output, shell=True, stdout=PIPE).stdout.read()
        print output

    def remove_from_hg(self):
        """Remove from Mercurial version control."""
        print """Removing migration from Mercurial..."""
        output = "hg remove %s" % (self.info['filename'])
        output = Popen(output, shell=True, stdout=PIPE).stdout.read()
        print output

    def rollback(self, verbose=True, message=''):
        """Roll back changes to latest state"""
        if verbose:
            print "---"
            print """Migration #%s has failed.""" % (self.info['migration']),
            if message:
                print message
            print """Check your migration file (%s) and correct it.""" % self.info['filename']
            print "Rolling back failed migration..."
        db.dump.load(self.state_to_rollback)
        raise MigrationFailedError(migration_number=self.info['migration'])
