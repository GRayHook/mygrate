#!/usr/bin/env python
"""Main executable of app"""
import sys
import cmds

def about_mygrate():
    """Represents app"""
    print """GRay_Mygrate - MySQL Sexy Migration Tool (forked by GRayHook)\n"""
    print """Available commands are:\n"""

    for cmd in sorted(cmds.__all__):
        if cmd in ['checkout']:
            continue
        module = "cmds.%s" % cmd
        __import__(module)
        print " %s %s" % (cmd.ljust(10, ' '), sys.modules[module].run.__doc__)

    print
    sys.exit()

def main():
    """Body of program"""

    aliases = {}

    for cmnd in cmds.__all__:
        for alias in [cmnd[:i+1] for i in range(len(cmnd) - 1)]:
            aliases[alias] = aliases[alias] + [cmnd] if (alias in aliases) else [cmnd]

    aliases['ci'] = ["commit"]
    aliases['co'] = ["update"]

    command = ""
    try:
        command = sys.argv[1]
        try:
            args = sys.argv[2:]
        except IndexError:
            args = []
        if command in aliases.keys():
            if len(aliases[command]) > 1:
                print """Mygrate: command '%s' is ambiguous:
                    %s""" % (command, " ".join(aliases[command]))
                sys.exit()
            [command] = aliases[command]
        if command in cmds.__all__:
            module = "cmds.%s" % command
            __import__(module)
            sys.modules[module].run(args)
        else:
            if command == 'help':
                about_mygrate()
            print "Unknown command: '%s'. For the list of",\
                  " available commands run 'mygrate help''" % command
            ValueError('Command not found')
    except (IndexError, KeyError):
        about_mygrate()

if __name__ == '__main__':
    main()
